import * as mongoose from 'mongoose';
const { Schema } = mongoose;

export const UserSchema = new Schema({
    name: String,
    email: String,
    password: String,
    confirm: String,
})
