import { Injectable } from '@nestjs/common';

import { User } from './user.entity';
import { UserDto } from './dto/UserDto';

@Injectable()
export class UserService {
    async create(userDto: UserDto) {
    const user = User.create(userDto);
    await user.save();

    delete user.password;
    return user;
    }

    async showById(id: number): Promise<User> {
    const user = await this.findById(id);

    delete user.password;
    return user;
    }

    async findById(id: number) {
    return await User.findOne(id);
    }

    async findByEmail(email: string) {
    return await User.findOne({
        where: {
            email: email,
        },
    });
    }
}