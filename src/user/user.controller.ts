import { Controller, Get, Post, Body, Param } from '@nestjs/common';

import { UserService } from './user.service';
import { UserDto } from './dto/UserDto';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Post()
    create(@Body() userDto: UserDto) {
    return this.userService.create(userDto);
    }

    @Get(':id')
    show(@Param('id') id: string) {
    return this.userService.showById(+id);
    }
}